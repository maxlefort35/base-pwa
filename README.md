![alt text](https://user-images.githubusercontent.com/3104648/28351989-7f68389e-6c4b-11e7-9bf2-e9fcd4977e7a.png "Logo Title Text 1")

# Base d'une PWA

Mise en place du manifest.json avec les infos de l'appli.

Appel du service-worker.js

Appel du sw-toolbox.js

Doc pour ce projet : 

- [PWA-Workshop](https://pwa-workshop.js.org/fr/) 
- [Vidéo Youtube de Raphael Rollet](https://www.youtube.com/watch?v=-5HGanW0yuA&ab_channel=RaphaelRollet) 
